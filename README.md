
SERVER OS
- Ubuntu 16.04.2 - server

RESOURCES SETUP
- sudo apt-get update
- sudo apt-get install postgresql postgresql-contrib
	<!-- switch to postgresql -->
	- sudo -u postgres psql
	- "\q" to exit
	<!-- start loading data from external .sql -->
- sudo apt-get install nodejs-legacy
	- node --version
- sudo apt-get install npm
	- npm
- sudo apt-get install git
	- git --version
- sudo npm install nodemon -g
- sudo npm install forever -g
- sudo apt-get install mongodb
	<!-- run mongodb -->
	- mongod
	<!-- access mongodb -->
	- mongo
	- show dbs
	- use [dbname]

<!-- init local repository though git -->
- sudo git clone https://github.com/narraressan/-teamNarra-project-X.git
- cd /root/-teamNarra-project-X.git
<!-- install dependencies -->
- sudo npm install

<!-- initialize mongodb -->
<!-- windows: -->
	+ create C:\data\db
	+ run mongod
	+ run mongo.exe
	+ show dbs;
	<!-- insert create syntax here... -->

<!-- running server process in background -->
- sudo forever start [RETS API SCRIPT]
- sudo forever list