

// import utilities
const reqExpress = require("express"); 
const reqIPFilter = require("express-ipfilter").IpFilter;
const reqbodyParser = require("body-parser");
const reqfileUpload = require("express-fileupload");
const reqPath = require("path");
const reqPsql = require("pg");
const reqFs = require("fs");
const reqUtil = require("util");
const reqCrypto = require("crypto");

// https://github.com/mongodb/node-mongodb-native
var reqMongo = require("mongodb").MongoClient;
var reqAssert = require("assert");



// custom console.log function to write text file for monitoring
	var logName = "projectx-backend-logs.txt";
	var logFile = reqFs.createWriteStream(logName, { flags: "a" });
	var logStdout = process.stdout;
	console.log = function () {
		logFile.write(reqUtil.format.apply(null, arguments) + "\n");
		logStdout.write(reqUtil.format.apply(null, arguments) + "\n");
	}
	console.error = console.log;
// --------------------------------------------------------



// default values
	var express = reqExpress();
	var config = {
		"default-port": 80,
		"blacklisted": [],
		"en-algorithm": "aes-256-ctr",
		"en-key": "2012-0129-2012-0422",
		// psql - remote
		"psql-addr": "162.243.211.61",
		"psql-dbname": "pxv1",
		"psql-user": "postgres",
		"psql-pass": "",
		// mongo - remote
		"mongo-addr": "162.243.211.61",
		"mongo-dbname": "pxv1",
		"mongo-port": "27017"
	};
	var psqlConnect = "postgres://" + config["psql-user"] + ":" + config["psql-pass"] + "@" + config["psql-addr"] + "/" + config["psql-dbname"];
	var mongoConnect = "mongodb://" + config["mongo-addr"] +":" + config["mongo-port"] + "/" + config["mongo-dbname"];
	// express.use(reqIPFilter(config["blacklisted"])); ---> use this when you intend to block an IP
	express.use(reqbodyParser.urlencoded({ extended: false, limit: '50mb' }));
	express.use(reqbodyParser.json({ limit: '50mb' }));
	express.use(reqfileUpload());
// --------------------------------------------------------



// default functions
	
	// NOTES:
	// encryption and decryption source: http://lollyrock.com/articles/nodejs-encryption/

	function encrypt(text){
		var cipher = reqCrypto.createCipher(config["en-algorithm"], config["en-key"]);
		var crypted = cipher.update(text, "utf8", "hex");
		crypted += cipher.final("hex");
		return crypted;
	}

	function decrypt(text){
		var decipher = reqCrypto.createDecipher(config["en-algorithm"], config["en-key"]);
		var dec = decipher.update(text, "hex", "utf8");
		dec += decipher.final("utf8");
		return dec;
	}

	function rand(){ return Math.random().toString(36).substr(2); };

	function assembleTime(number){ 
		if(number <= 9 && number >= 0){ return "0" + number; }
		else{ return number; }
	}

	var nextTrigger = 0;
	function initTrigger(countDown){
		// manipulate here to force trigger
		// if((parseInt(countDown[0])) == 17){ return true; }

		// check - 4 am - 8 pm - 8 pm - 
		var curr = parseInt(countDown[0])
		if(curr == 6 || curr == 12 || curr == 18 || curr == 24){ return true; }
		else{ 
			if(curr < 6){ nextTrigger = 6; }
			else if(curr >= 6 && curr < 12){ nextTrigger = "12:00"; }
			else if(curr >= 12 && curr < 18){ nextTrigger = "18:00"; }
			else if(curr >= 18 && curr < 24){ nextTrigger = "24:00"; }
			else { nextTrigger = "Waiting"; }

			return false; 
		}
		
		return true;
	}

	function psql_throwQuery(callback){
		// Asynch implementation of psql queries...
		reqPsql.connect(psqlConnect, function(err, client, done){ callback(err, client, done); });
	}

	function psql_generalError(err, type, res){ 
		if(type == 1){ res.send("Error fetching client from pool"); }
		else if(type == 2){ 
			var note = "Error during query - ";
			if(err != null){ 
				if(err["detail"] != undefined){ return note + err["detail"]; }
				else{ return note + err; }
			}
			else{ return note + " unknown"; }
		}
		else if(type == 3){ return "Error query. Check missing/incorrect credential or contact support."; }
		else{ return "Unknown error trigger! Send Help!."; }
	}

	function mongo_throwQuery(callback){
		// Asynch implementation of mongo queries...
		reqMongo.connect(mongoConnect, function(err, db){ callback(err, db); });
	}
// --------------------------------------------------------



// RESTful endpoints
	// connection test
		express.get("/projectx-server/check-activity", (req, res) => {
			res.send("Project - X - server active - " + new Date());
		});

	// TRIGGER TIME - all jobs sync to server clock
		// get server time for identifying triggers
			express.get("/projectx-server/check-uniform-time", (req, res) => {
				var owner = req.query["client"];

				// sending triggers every - 10 a.m. server time
				var countDown = new Date().toTimeString().split(" ")[0].split(":");
				var temp = {
					time: assembleTime(parseInt(countDown[0])) + ":" + assembleTime(parseInt(countDown[1])) + ":" + assembleTime(parseInt(countDown[2])),
					status: initTrigger(countDown),
					date: (new Date().toISOString().substring(0, 10)),
					// date: "2017-04-02",
					schedule: [],
					next: nextTrigger,
				}

				if(temp["status"]){
					// attach schedule here - only if you haven't triggered for today
					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							client.query("SELECT distinct a._leadfinder, a._title, c._content, a._trigger, a._recipients, to_char(max(b._date), 'YYYY-MM-DD') as _latest FROM pxv1_leadfinder a LEFT JOIN pxv1_leadfinder_log b ON a._leadfinder = b._leadfinder LEFT JOIN pxv1_emailtemplates c ON a._template = c._template WHERE a._token = $1 AND a._active = true GROUP BY a._leadfinder, a._title, c._content, a._trigger, a._recipients", [owner], function (err, result) {
								done();

								if (!err && result.rowCount != 0) { temp["schedule"] = result.rows; }
								res.send(temp);
							});
						}
					});
				}
				else{ res.send(temp); }
			});

		// log all triggers
			express.get("/projectx-server/log-trigger", (req, res) => {
				var responseData = { "message": null, "data": null };

				if(req.query["leadfinder"] != "" || req.query["leadfinder"] != undefined){
					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							client.query("INSERT INTO pxv1_leadfinder_log(_date, _leadfinder) VALUES ($1, $2);", [req.query["date"], req.query["leadfinder"]], function (err, result) {
								done();

								if (!err) { responseData["message"] = "Logging success."; }
								res.send(responseData);
							});
						}
					});
				}
				else{ 
					responseData["message"] = "Error logging trigger. Contact your admin.";
					res.send(responseData); 
				}
			});

	// PLANS
		// create NEW SUBSCRIPTION/PLAN
			express.post("/projectx-server/new-plan", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				// ADMIN ONLY FUNCTION
				if(auth == config["en-key"]){
					var token = rand() + rand();

					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							client.query("INSERT INTO pxv1_plans(_subscription, _package, _price_text, _duration_text, _details) VALUES ($1, $2, $3, $4, $5);", [token, reqData["package"], reqData["price"], reqData["duration"], reqData["details"]], function (err, result) {
								done();

								if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
								else{ responseData["message"] = "Query successful."; }
								res.send(responseData);
							});
						}
					})
				}
				else{
					responseData["message"] = "Validating authorization sequence failed."; 
					res.send(responseData);
				}
			});

		// get ALL SUBSCRIPTIONS
			express.get("/projectx-server/get-plans", (req, res) => {
				var responseData = { "message": null, "data": null };

				psql_throwQuery(function (err, client, done) {  
					if (err) { psql_generalError(err, 1, res); }
					else{
						client.query("SELECT * FROM pxv1_plans WHERE _active = true;", [], function (err, result) {
							done();

							if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
							else{ 
								if(result.rowCount == 0){ responseData["message"] = "There are no active PLANS available."; }
								else{ responseData["message"] = "List of active PLANS."; }
							
								responseData["data"] = result.rows;
							}
							res.send(responseData);
						});
					}
				});
			});

	// CLIENTS
		// register NEW CLIENT
			express.post("/projectx-server/new-client", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				// ADMIN ONLY FUNCTION
				if(auth == config["en-key"]){
					var token = rand() + rand();

					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							client.query("INSERT INTO pxv1_client(_token, _subscription, _email, _password, _details) VALUES ($1, $2, $3, $4, $5);", [token, reqData["subscription"], reqData["email"], encrypt(reqData["password"]), reqData["details"]], function (err, result) {
								done();

								if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
								else{ 
									responseData["message"] = "Query successful. Here is your license number."; 
									responseData["data"] = token; 
								}
								res.send(responseData);
							});
						}
					})
				}
				else{
					responseData["message"] = "Validating authorization sequence failed."; 
					res.send(responseData);
				}
			});

		// activate NEW CLIENT
			express.post("/projectx-server/activate-client", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				// USER ONLY FUNCTION
				if(auth != "" && auth != null && auth != "undefined"){
					var token = rand() + rand();

					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							// check if _token & email exists in the db (this account is inserted upon purchase)
							client.query("SELECT * FROM pxv1_client WHERE _token = $1", [auth], function (err, result) {
								done();

								if (err) { 
									responseData["message"] =  psql_generalError(err, 2, null); 
									res.send(responseData);
								}
								else{ 
									// check if the email it has is the same as current user
									if(result.rowCount == 1 && result.rows[0]["_email"] == reqData["email"] && result.rows[0]["_password"] == encrypt(reqData["password"])){
										// activate the token
										client.query("UPDATE pxv1_client SET _active = true WHERE _token = $1 AND _email = $2 AND _password = $3", [auth, reqData["email"], encrypt(reqData["password"])], function (err, result) {
											done();

											if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
											else{ 
												responseData["message"] = "Query successful."; 
												responseData["data"] = 200;
											}
											res.send(responseData);
										});
									}
									else{ 
										responseData["message"] =  psql_generalError(err, 3, null);
										res.send(responseData);
									}
								}
							});
						}
					})
				}
				else{
					responseData["message"] = "Unable to register key."; 
					res.send(responseData);
				}
			});

		// get one user
			express.get("/projectx-server/get-client", (req, res) => {
				var responseData = { "message": null, "data": null };

				var license = req.query["client"];

				psql_throwQuery(function (err, client, done) {  
					if (err) { psql_generalError(err, 1, res); }
					else{
						client.query("SELECT * FROM pxv1_client WHERE _token = $1;", [license], function (err, result) {
							done();

							if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
							else{ 
								if(result.rowCount == 0){ responseData["message"] = "There no user of this license."; }
								else{ responseData["message"] = "User details."; }
								responseData["data"] = result.rows;
							}
							res.send(responseData);
						});
					}
				});
			});

	// EMAIL WRITER
		// UPSERT email templates
			express.post("/projectx-server/in-client/upsert-email-template", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				if(auth != "" && auth != null && auth != "undefined"){
					if(reqData["template"] == "" || reqData["template"] == "undefined" || reqData["template"] == null){ 
						reqData["template"] = rand() + rand(); 
					}

					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							client.query("INSERT INTO pxv1_emailtemplates(_token, _template, _title, _content, _details) VALUES ($1, $2, $3, $4, $5) ON CONFLICT (_template, _token) DO UPDATE SET _title = $6 , _content = $7 , _details = $8 , _active = $9;", [auth, reqData["template"], reqData["title"], reqData["message"], reqData["details"], reqData["title"], reqData["message"], reqData["details"], reqData["active"]], function (err, result) {
								done();

								if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
								else{ 
									responseData["message"] = "UPSERT on template " + reqData["title"] + " successful."; 
									responseData["data"] = reqData["template"];
								}
								res.send(responseData);
							});
						}
					})
				}
				else{
					responseData["message"] = "Validating authorization sequence failed."; 
					res.send(responseData);
				}
			});

		// delete (deactivate) email template
			express.post("/projectx-server/in-client/delete-email-template", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				if(auth != "" && auth != null && auth != "undefined"){
					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							client.query("UPDATE pxv1_emailtemplates SET _active = false WHERE _token = $1 AND _template = $2;", [auth, reqData["template"]], function (err, result) {
								done();
								
								if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
								else{ responseData["message"] = "Template deleted"; }
								res.send(responseData);
							});
						}
					})
				}
				else{
					responseData["message"] = "Validating authorization sequence failed."; 
					res.send(responseData);
				}
			});

		// get one/all emails per user
			express.get("/projectx-server/get-email-templates", (req, res) => {
				var responseData = { "message": null, "data": null };

				var owner = req.query["owner"];
				var set = req.query["set"];

				psql_throwQuery(function (err, client, done) {  
					if (err) { psql_generalError(err, 1, res); }
					else{
						sql = "SELECT * FROM pxv1_emailtemplates WHERE _active = true AND _token = $1";
						values = [owner];

						if(set != "*"){
							sql += " AND _template = $2";
							values.push(set);
						}

						sql += " ORDER BY _log_created DESC;";

						client.query(sql, values, function (err, result) {
							done();

							if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
							else{ 
								if(result.rowCount == 0){ responseData["message"] = "There are no active templates available."; }
								else{ responseData["message"] = "List of active templates."; }
							
								responseData["data"] = result.rows;
							}
							res.send(responseData);
						});
					}
				});
			});

	// LEAD FINDER
		// UPSERT lead finder - schedule
			express.post("/projectx-server/in-client/upsert-lead-finder", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				if(auth != "" && auth != null && auth != "undefined"){
					if(reqData["lead"] == "" || reqData["lead"] == "undefined" || reqData["lead"] == null){ reqData["lead"] = rand() + rand(); }

					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							client.query("INSERT INTO pxv1_leadfinder(_token, _leadfinder, _title, _template, _trigger, _recipients) VALUES ($1, $2, $3, $4, $5, $6) ON CONFLICT (_token, _leadfinder, _template, _trigger) DO UPDATE SET _title = $7 , _template = $8 , _trigger = $9 , _recipients = $10;", [auth, reqData["lead"], reqData["title"], reqData["template_id"], reqData["trigger"], JSON.stringify(reqData["recipients"]), reqData["title"], reqData["template_id"], reqData["trigger"], JSON.stringify(reqData["recipients"])], function (err, result) {
								done();

								if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
								else{ 
									responseData["message"] = "UPSERT on schedule " + reqData["title"] + " successful."; 
									responseData["data"] = reqData["lead"];
								}
								res.send(responseData);
							});
						}
					})
				}
				else{
					responseData["message"] = "Validating authorization sequence failed."; 
					res.send(responseData);
				}
			});

		// UINSERT with XPRESS - 
			express.post("/projectx-server/in-client/insert-xpress-lead-finder", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				if(auth != "" && auth != null && auth != "undefined"){
					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{

							// construct query string
							var countG = 1;
							var queryString = "INSERT INTO pxv1_leadfinder(_token, _leadfinder, _title, _template, _trigger, _recipients) VALUES ";
							var paramArr = [];
							var newLeads = [];

							for (var i = 0; i < reqData.length; i++) {
								var qTemp = "($"+ (countG++) +", $"+ (countG++) +", $"+ (countG++) +", $"+ (countG++) +", $"+ (countG++) +", $"+ (countG++) +")";

								leadToken = rand() + rand();
								newLeads.push(leadToken);

								paramArr.push(auth);
								paramArr.push(leadToken);
								paramArr.push(reqData[i]["title"]);
								paramArr.push(reqData[i]["template"]);
								paramArr.push(reqData[i]["trigger"]);
								paramArr.push(JSON.stringify(reqData[i]["recipient"]));

								queryString += qTemp;

								if(i != (reqData.length-1)){ queryString += ", "; }
							}

							client.query(queryString, paramArr, function (err, result) {
								done();

								if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
								else{ 
									responseData["message"] = "Success! New Schedules has been created through XPRESS."; 
									responseData["data"] = newLeads;
								}
								res.send(responseData);
							});
						}
					})
				}
				else{
					responseData["message"] = "Validating authorization sequence failed."; 
					res.send(responseData);
				}
			});

		// delete (deactivate) lead finder - schedule
			express.post("/projectx-server/in-client/delete-lead-finder", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				if(auth != "" && auth != null && auth != "undefined"){
					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							client.query("UPDATE pxv1_leadfinder SET _active = false WHERE _token = $1 AND _leadfinder = $2;", [auth, reqData["lead"]], function (err, result) {
								done();
								
								if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
								else{ responseData["message"] = "Schedule deleted"; }
								res.send(responseData);
							});
						}
					})
				}
				else{
					responseData["message"] = "Validating authorization sequence failed."; 
					res.send(responseData);
				}
			});

		// get one/all lead finder - schedule
			express.get("/projectx-server/get-lead-finder-schedules", (req, res) => {
				var responseData = { "message": null, "data": null };

				var owner = req.query["owner"];
				var set = req.query["set"];

				psql_throwQuery(function (err, client, done) {  
					if (err) { psql_generalError(err, 1, res); }
					else{
						sql = "SELECT a.*, b._event, c._title as _email, c._details FROM pxv1_leadfinder a, pxv1_trigger b, pxv1_emailtemplates c WHERE a._active = true AND b._active = true AND c._active = true AND a._trigger = b._trigger AND a._template = c._template AND a._token = $1";
						values = [owner];

						if(set != "*"){
							sql += " AND a._leadfinder = $2;";
							values.push(set);
						}

						sql += " ORDER BY a._log_created DESC;";

						client.query(sql, values, function (err, result) {
							done();

							if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
							else{ 
								if(result.rowCount == 0){ responseData["message"] = "There are no active schedules available."; }
								else{ responseData["message"] = "List of active schedules."; }
							
								responseData["data"] = result.rows;
							}
							res.send(responseData);
						});
					}
				});
			});

		// log on trigger - save trigger data (e.g. success and failed triggers) ----> pxv1_archive > pxv1_raw_leadfinder

	// TRIGGERS
		// get trigger/s
			express.get("/projectx-server/get-triggers", (req, res) => {
				var responseData = { "message": null, "data": null };

				psql_throwQuery(function (err, client, done) {  
					if (err) { psql_generalError(err, 1, res); }
					else{
						client.query("SELECT * FROM pxv1_trigger WHERE _active = true;", [], function (err, result) {
							done();

							if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
							else{ 
								if(result.rowCount == 0){ responseData["message"] = "There are no triggers registered."; }
								else{ responseData["message"] = "Trigger options."; }
								responseData["data"] = result.rows;
							}
							res.send(responseData);
						});
					}
				});
			});

	// ANALYTICS
		// new analytics log
			express.post("/projectx-server/new-analytics-log", (req, res) => {
				var responseData = { "message": null, "data": null };

				var auth = req.header("Authorization");
				var reqData = req.body;

				if(auth != "" && auth != null && auth != "undefined"){
					psql_throwQuery(function (err, client, done) {  
						if (err) { psql_generalError(err, 1, res); }
						else{
							client.query("INSERT INTO pxv1_analytics(_token, _func_author, _nth, _status) VALUES ($1, $2, $3, $4);", [auth, reqData["funcAuthor"], JSON.stringify(reqData["nth"]), reqData["status"]], function (err, result) {
								done();

								if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
								else{ 
									responseData["message"] = "State added to analytics."; 
									responseData["data"] = reqData["funcAuthor"]; 
								}
								res.send(responseData);
							});
						}
					})
				}
				else{
					responseData["message"] = "Validating authorization sequence failed."; 
					res.send(responseData);
				}
			});

		// get analytics log
			express.get("/projectx-server/get-analytics-log", (req, res) => {
				var responseData = { "message": null, "data": null };

				var owner = req.query["owner"];
				var dates = req.query["dates"];

				var values = [owner].concat(dates);

				psql_throwQuery(function (err, client, done) {  
					if (err) { psql_generalError(err, 1, res); }
					else{
						client.query("SELECT a.*, count(*) as score FROM (SELECT _func_author as func, _status as status, to_char(_log_created, 'YYYY-MM-DD') as date from pxv1_analytics WHERE _token = $1) as a WHERE date in ($2, $3, $4, $5, $6, $7, $8) group by func, status, date ORDER BY date ASC", values, function (err, result) {
							done();

							if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
							else{ 
								if(result.rowCount == 0){ responseData["message"] = "There are no logs available for -" + owner; }
								else{ responseData["message"] = "List of analytic logs - " + owner; }
							
								responseData["data"] = result.rows;
							}
							res.send(responseData);
						});
					}
				});
			});

		// get analytics log - daily
			express.get("/projectx-server/get-analytics-daily", (req, res) => {
				var responseData = { "message": null, "data": null };

				var dataSet = req.query["dataSet"];

				psql_throwQuery(function (err, client, done) {  
					if (err) { psql_generalError(err, 1, res); }
					else{
						client.query("SELECT * FROM (SELECT _func_author as func, _nth as nth, _status as status, to_char(_log_created, 'YYYY-MM-DD') as date from pxv1_analytics WHERE _token = $1) as a WHERE date = $2 AND func = $3 ORDER BY date ASC", dataSet, function (err, result) {
							done();

							if (err) { responseData["message"] =  psql_generalError(err, 2, null); }
							else{ 
								if(result.rowCount == 0){ responseData["message"] = "There are no logs available for -" + JSON.stringify(dataSet); }
								else{ responseData["message"] = "List of analytic logs - " + JSON.stringify(dataSet); }
							
								responseData["data"] = result.rows;
							}
							res.send(responseData);
						});
					}
				});
			});

// --------------------------------------------------------



// Proper error handling
	express.use(function(req, res){ res.status(400); });
	express.use(function(error, req, res) { res.status(500); });
// --------------------------------------------------------




// default instance
	express.listen(config["default-port"], () => { 
		// check if express is running
		console.log("listening to port " + config["default-port"]);

		// check psql is running
		psql_throwQuery(function (err, client, done) {  
			if (err) { return console.error('error fetching client from pool', err); }
			else { console.log("psql running @" + config["psql-addr"]); }
		});

		// check if mongodb is running
		// mongo_throwQuery(function(err, db) {
		// 	reqAssert.equal(null, err);
		// 	console.log("mongo running @" + config["mongo-addr"]);
		// 	db.close();
		// });
	});
// --------------------------------------------------------