
// default config settings
	const config = JSON.parse(conf);
	const global_server = config["protocol"] + "://" + config["address"] + ":" + config["port"];
	// const twitterconfig = JSON.parse(twit);
	const twitterconfig = JSON.parse("{}");

// nodejs
	var fs = require("fs");
	// var d3 = require("d3v3");

// Vuew instance
var projectx = new Vue({
	el: "#projectx-main-vue",
	data: {
		// cookies - THIS IS A PRIVACY ISSUE
			cookies: {
				linkedin: "",
				twitter: "",
				facebook: "",
				pinterest: "",
			},
			linkedin_spyders: {
				linkedin_recipients: [],
				linkedin_networkRecipients: [],
				// linkedin_selected: [],
				search_recipients: "",
				linkedin_networkPageRange: 1000,
				search_networkRecipients: "",
				search_filter: "List(v->PEOPLE)",
				search_startCount: 0,
				search_total: 0,
			},
		// view variables
			navStatus: false,
			activeView: "",
		// general settings
			generalSettings: {
				license: "",
				email: "",
				password: "",
				status: false,
				remoteStatus: false
			},
		// email template
			emailTemplates: [],
			currentTemplate: {
				template: "",
				title: "",
				message: "",
				details: "",
				active: true
			},
			search_emailTemplate: "",
		// lead finder
			scheduleList: [],
			currentSchedule_LeadFinder_sendConnection: {
				lead: "",
				title: "",
				template_id: "",
				recipients: {
					searchKey: "",
					n_recipients: 0,
				},
				trigger: ""
			},
			currentSchedule_LeadFinder_sendMessage: {
				lead: "",
				title: "",
				template_id: "",
				recipients: [],
				trigger: ""
			},
			triggerOptions: [],
		// trigger factor
			triggerCountDown: "",
			triggerToday: false,
			today: "",
			inviteeCollection: [],
			nextTrigger: "",
			myTime: new Date(),
			enableRefresh: true,
		// Graphs
			invites_date: (new Date().toISOString().substring(0, 10)),
			invites_data: [0, 0, 0],
			invites_data_users: [],
			sendMessage_date: (new Date().toISOString().substring(0, 10)),
			sendMessage_data: [0, 0, 0],
			sendMessage_data_users: [],
			profileView_date: (new Date().toISOString().substring(0, 10)),
			profileView_data: [0, 0, 0],
			profileView_data_users: [],
			timeLine_Data: {
				labels: [],
				Invites: [0, 0, 0, 0, 0, 0, 0],
				Messages: [0, 0, 0, 0, 0, 0, 0],
				VProfile: [0, 0, 0, 0, 0, 0, 0]
			},
		// xpress
			invite_listText: "",
			invite_listLength: 0,
			xpress_view: "xpress-Step1",
			xpress_invites: [],
	},
	methods: {
		// view render
			showView: function(view){ 
				console.log(view);
				$('[data-toggle="popover"]').popover('hide');
				$('[data-ident="steps"]').popover('hide');


				$('#xpressModal').on('hidden.bs.modal', function () { projectx.hideSteps(); });

				this.activeView = view; 
				// this is for dynamic events - Ex: popover / tooltip etc.
				this.$nextTick(
					function () { 
						$('[data-toggle="popover"]').popover({ html: true }); 
						$('[data-ident="steps"]').popover({ html: true });
					}
				);

				// if(view == "home"){
				// 	// find the starting point of the Xpress!
				// 	if(this.generalSettings['status'] != true){ this.xpress_view = "xpress-Step1"; }
				// 	else if(this.cookies["linkedin"].trim() != "" || this.cookies["linkedin"] != null || this.cookies["linkedin"] != "undefined"){ this.xpress_view = "xpress-Step2"; }
				// 	else{ this.xpress_view = "xpress-Step2"; }
				// }
			},
			openNav: function(){
				document.getElementById("projectx-sidenav").className += " active";
				document.getElementById("projectx-content").className += " active";
				this.navStatus = true;
			},
			closeNav: function(){
				document.getElementById("projectx-sidenav").classList.remove("active");
				document.getElementById("projectx-content").classList.remove("active");
				this.navStatus = false;
			},
			toggleNav: function(){
				if(this.navStatus == false){ this.openNav(); }
				else{ this.closeNav(); }

				this.hideSteps();
			},
			showSteps: function(){ 
				this.hideSteps();
				$('[data-ident="steps"]').popover('toggle'); 
			},
			showSpecificSteps: function(arr){ 
				for (var i = 0; i < arr.length; i++) {
					$('[data-id="'+ arr[i] +'"]').popover('toggle'); 
				}
			},
			showStep: function(el){ 
				this.hideSteps();
				$('[data-id="' + el + '"]').popover('toggle'); 
			},
			hideStep: function(el){  
				$('[data-id="' + el + '"]').popover('hide');
				$('[data-toggle="popover"]').popover('hide');
			},
			hideSteps: function(){  
				$('[data-toggle="popover"]').popover({ html: true }); 
				$('[data-ident="steps"]').popover({ html: true });

				$('[data-ident="steps"]').popover('hide'); 
				$('[data-toggle="popover"]').popover('hide');
			},
		// tour
			tourStep: function(show){
				this.hideSteps();
				this.showStep(show);
			}, 
		// settings handler
			checkUserValidity: function(){
				// check user validity
				NProgress.start();
				$.ajax({
					method: "get",
					url: global_server + "/projectx-server/get-client",
					contentType: "application/json",
					data: { client: this.generalSettings["license"] }
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"][0]["_active"]){ projectx.generalSettings["remoteStatus"] = true; }
					else{ projectx.generalSettings["remoteStatus"] = false; }

					NProgress.done();
				});
			},
			activateUser: function(){
				if(this.generalSettings["email"].trim() == "" || this.generalSettings["password"].trim() == "" || this.generalSettings["license"].trim() == ""){
					alert("Either your email/password/license is missing.");
				}
				else{
					// activate user
					NProgress.start();
					$.ajax({
						method: "post",
						url: global_server + "/projectx-server/activate-client",
						contentType: "application/json",
						headers: { "Authorization": this.generalSettings["license"] },
						data: JSON.stringify({ email: this.generalSettings["email"], password: this.generalSettings["password"] })
					})
					.done(function(data, textStatus, xhr){
						// console.log(data);
						// console.log(textStatus);
						// console.log(xhr);

						if(data["data"] != 200){ alert(data["message"]); }
						else{ projectx.loadCredentials(); }
						NProgress.done();
					});
				}
			},
			resetCredentials: function(location){
				this.saveCredentials("", location);
				this.generalSettings['remoteStatus'] = false;
				this.getEmailTemplates();
				this.this.get_leadFinders();
			},
			saveCredentials: function(saveContent, location){
				// var saveContent = "{ \"license\": \"" + this.generalSettings["license"] + "\",\"email\": \"" + this.generalSettings["email"] + "\"}";

				NProgress.start();
				fs.writeFile(__dirname + location, saveContent.replace(/'/g, "\""), function(err){
					if(err){ alert("Saving crediantials failed."); }
					else{
						// refresh all credentials for consistency
						projectx.loadCredentials();
					}
					NProgress.done();
				});
			},
			loadCredentials: function(){
				fs.readFile(__dirname + "/../config/local.json", "utf8", (err, data) => {
					if(err){ 
						alert("No account associated with this software is detected. Please register and activate your account now."); 
						this.activeView = "settings";
					}
					else{
						try{
							var storedData = JSON.parse(data);
							projectx.generalSettings["license"] = storedData["license"];

							if(projectx.generalSettings["license"].trim() != ""){
								projectx.generalSettings["status"] = true;
								projectx.checkUserValidity();
								projectx.getEmailTemplates();
								projectx.get_leadFinders();
								projectx.getAnalytics();

								projectx.getAnalytics_daily(projectx.invites_date, "sendInvite", projectx.invites_data, projectx.invites_data_users);
								projectx.getAnalytics_daily(projectx.sendMessage_date, "sendMessage", projectx.sendMessage_data, projectx.sendMessage_data_users);
								projectx.getAnalytics_daily(projectx.profileView_date, "profileView", projectx.profileView_data, projectx.profileView_data_users);

								// you need to be in the app HTML NOT in external website!
								// so the clock can trigger - 
								projectx.syncClock();
							}
						}
						catch(Exception){
							projectx.generalSettings["license"] = "";
							projectx.generalSettings["status"] = false;
							alert("Error loading credentials. It's either empty/deleted/unformatted.");
						}
					}
				});
			},
		// cookie handler
			loadCookieFile: function(){
				// THIS IS A PRIVACY ISSUE!
				this.cookies["linkedin"] = fs.readFileSync(__dirname + "/../config/cookie.txt", "utf8");
			},
			getCookie: function(cname, thisCookies) {
				var name = cname + "=";
				var ca = thisCookies.split(";");
				for (var i = 0; i < ca.length; i++) {
					ca[i] = ca[i].trim();
					if(ca[i].indexOf(cname) != -1){
						var temp = ca[i].split(cname + "=")[1];
						return temp.substring(1, temp.length-1);
					}
				}
			},
			liveSearchLinkedin: function(startCount){
				var searchTime = (new Date().getTime());
				var searchString = encodeURI(this.linkedin_spyders["search_recipients"]);

				if(startCount == "next"){ this.linkedin_spyders["search_startCount"] = this.linkedin_spyders["search_startCount"] + 10; }
				else if(startCount == "prev"){ 
					if(this.linkedin_spyders["search_startCount"] >= 10 ){
						this.linkedin_spyders["search_startCount"] = this.linkedin_spyders["search_startCount"] - 10; 
					}
				}
				else{
					try{ this.linkedin_spyders["search_startCount"] = parseInt(startCount); }
					catch(err){ this.linkedin_spyders["search_startCount"] = 0; }
				}

				NProgress.start();

				var searchMethod = "SWITCH_SEARCH_VERTICAL";
				if(this.linkedin_spyders["search_filter"] == "List()"){ searchMethod = "GLOBAL_SEARCH_HEADER"; }

				$.ajax({
					method: "get",
					url: "https://www.linkedin.com/voyager/api/search/cluster?count=10&guides="+encodeURI(this.linkedin_spyders["search_filter"])+"&keywords="+searchString+"&origin="+searchMethod+"&q=guided&searchId="+searchTime+"&start=" + this.linkedin_spyders["search_startCount"],
					headers: {
						"csrf-token": this.getCookie("JSESSIONID", this.cookies["linkedin"]),
						"x-li-track":'{"clientVersion":"1.0.*","osName":"web","clientTimestamp":'+searchTime+',"timezoneOffset":8,"deviceFormFactor":"DESKTOP"}'
					}
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data['elements'].length > 0){
						projectx.linkedin_spyders["linkedin_recipients"] = data["elements"][0]["elements"];
						projectx.linkedin_spyders["search_total"] = data["paging"]["total"];
						projectx.linkedin_spyders["search_startCount"] = data["paging"]["start"];
					}
					else{
						projectx.linkedin_spyders["linkedin_recipients"] = [];
						projectx.linkedin_spyders["search_total"] = 0;
						projectx.linkedin_spyders["search_startCount"] = 0;
					}
					NProgress.done();
				});
			},
			// you can only have max 30,000 connections in linkedin
			// this querry can only handle 2000 max
			liveSearchLinkedin_getNetwork: function(startCount){
				var searchTime = (new Date().getTime());

				if(startCount == "next"){ this.linkedin_spyders["search_countNetwork"] = this.linkedin_spyders["search_countNetwork"] + this.linkedin_spyders["linkedin_networkPageRange"]; }
				else if(startCount == "prev"){ 
					if(this.linkedin_spyders["search_countNetwork"] >= this.linkedin_spyders["linkedin_networkPageRange"] ){
						this.linkedin_spyders["search_countNetwork"] = this.linkedin_spyders["search_countNetwork"] - this.linkedin_spyders["linkedin_networkPageRange"]; 
					}
				}
				else{
					try{ this.linkedin_spyders["search_countNetwork"] = parseInt(startCount); }
					catch(err){ this.linkedin_spyders["search_countNetwork"] = 0; }
				}

				NProgress.start();
				$.ajax({
					method: "get",
					url: "https://www.linkedin.com/voyager/api/relationships/connections?count=" + this.linkedin_spyders["linkedin_networkPageRange"] + "&start=" + this.linkedin_spyders["search_countNetwork"] + "&keyword=" + this.linkedin_spyders["search_networkRecipients"],
					headers: {
						"csrf-token": this.getCookie("JSESSIONID", this.cookies["linkedin"]),
						"x-li-track":'{"clientVersion":"1.0.*","osName":"web","clientTimestamp":'+searchTime+',"timezoneOffset":8,"deviceFormFactor":"DESKTOP"}'
					}
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data['elements'].length > 0){
						projectx.linkedin_spyders["linkedin_networkRecipients"] = data["elements"];
						projectx.linkedin_spyders["network_total"] = data["metadata"]["id"];
						projectx.linkedin_spyders["search_countNetwork"] = data["paging"]["start"];
					}
					else{
						projectx.linkedin_spyders["linkedin_networkRecipients"] = [];
						projectx.linkedin_spyders["network_total"] = 0;
						projectx.linkedin_spyders["search_countNetwork"] = 0;
					}

					NProgress.done();
				});
			},
		// email templates
			upsertEmailTemplate: function(){
				// upsert email template
				if(this.currentTemplate["title"].trim() != ""){
					NProgress.start();
					$.ajax({
						method: "post",
						url: global_server + "/projectx-server/in-client/upsert-email-template",
						contentType: "application/json",
						headers: { "Authorization": this.generalSettings["license"] },
						data: JSON.stringify({
							template: this.currentTemplate["template"],
							title: this.currentTemplate["title"],
							message: this.currentTemplate["message"].replace(/\n/g, "<br>").replace(/\t/g, "&emsp;").trim(),
							details: { comments: this.currentTemplate["details"].replace(/\n/g, "<br>").replace(/\t/g, "&emsp;").trim() },
							active: true
						})
					})
					.done(function(data, textStatus, xhr){
						// console.log(data);
						// console.log(textStatus);
						// console.log(xhr);

						alert(data["message"]);

						// clear current template
						if(data["data"] != null){ projectx.unstageTemplate(); }

						projectx.getEmailTemplates();
						NProgress.done();
					});
				}
				else{ alert("Please provide a proper template title."); }
			},
			deleteEmailTemplate: function(nth){
				// delete email template
				if (confirm("Are you sure to delete \"" + nth["_title"] + "\"? This will also delete schedules associated with this template.")) {
					NProgress.start();
					$.ajax({
						method: "post",
						url: global_server + "/projectx-server/in-client/delete-email-template",
						contentType: "application/json",
						headers: { "Authorization": this.generalSettings["license"] },
						data: JSON.stringify({ template: nth["_template"] })
					})
					.done(function(data, textStatus, xhr){
						// console.log(data);
						// console.log(textStatus);
						// console.log(xhr);

						alert(data["message"]);

						projectx.getEmailTemplates();
						NProgress.done();
					});
				}
			},
			getEmailTemplates: function(){
				// get email templates
				NProgress.start();
				$.ajax({
					method: "get",
					url: global_server + "/projectx-server/get-email-templates",
					contentType: "application/json",
					data: { owner: this.generalSettings["license"], set: "*" }
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					projectx.emailTemplates = data["data"];
					NProgress.done();
				});
			},
			stageTemplate: function(nth){
				try{
					this.currentTemplate["template"] = nth["_template"];
					this.currentTemplate["title"] = nth["_title"];
					this.currentTemplate["message"] = nth["_content"].replace(/<br>/g, "\n").replace(/&emsp;/g, "\t");
					this.currentTemplate["details"] = JSON.parse(nth["_details"])["comments"].replace(/<br>/g, "\n").replace(/&emsp;/g, "\t");
					this.currentTemplate["active"] = nth["_active"];
				}
				catch(err){
					// details dont have any comments
				}
			},
			unstageTemplate: function(){
				this.currentTemplate["template"] = "";
				this.currentTemplate["title"] = "";
				this.currentTemplate["message"] = "";
				this.currentTemplate["details"] = "";
				this.currentTemplate["active"] = true;
			},
		// leadfinder handler
			// testingSelected: function(nth){ console.log(this.linkedin_spyders['linkedin_selected']); },
			upsert_leadFinder: function(type){ 
				var toSave = {
					status: false,
					data: null
				};

				if(type == "connInvite"){
					if(this.currentSchedule_LeadFinder_sendConnection["title"].trim() != "" &&
						this.currentSchedule_LeadFinder_sendConnection["template_id"].trim() != "" &&
						this.currentSchedule_LeadFinder_sendConnection["trigger"].trim() != "" &&
						this.currentSchedule_LeadFinder_sendConnection["recipients"]["searchKey"].trim() != "" &&
						(typeof this.currentSchedule_LeadFinder_sendConnection["recipients"]["n_recipients"] === 'number' &&
							this.currentSchedule_LeadFinder_sendConnection["recipients"]["n_recipients"] > 0)){
						toSave["status"] = true;
						toSave["data"] = this.currentSchedule_LeadFinder_sendConnection;
					}
					else{ alert("Please complete the schedule fields before proceeding. Thank you."); }
				}
				else if(type == "InNetMessages"){
					if(this.currentSchedule_LeadFinder_sendMessage["title"].trim() != "" &&
						this.currentSchedule_LeadFinder_sendMessage["template_id"].trim() != "" &&
						this.currentSchedule_LeadFinder_sendMessage["trigger"].trim() != "" &&
						this.currentSchedule_LeadFinder_sendMessage["recipients"].length > 0){
						toSave["status"] = true;
						toSave["data"] = this.currentSchedule_LeadFinder_sendMessage;
					}
					else{ alert("Please complete the schedule fields before proceeding. Thank you."); }
				}

				// send ajax here - after ajax = set "toSave" to default
				if(toSave["status"] == true){
					NProgress.start();
					$.ajax({
						method: "post",
						url: global_server + "/projectx-server/in-client/upsert-lead-finder",
						contentType: "application/json",
						headers: { "Authorization": this.generalSettings["license"] },
						data: JSON.stringify(toSave["data"])
					})
					.done(function(data, textStatus, xhr){
						// console.log(data);
						// console.log(textStatus);
						// console.log(xhr);

						alert(data["message"]);

						// clear current template
						if(data["data"] != null){ projectx.unstage_leadFinder(); }

						projectx.get_leadFinders();
						NProgress.done();
					});
				}
			},
			delete_leadFinder: function(nth){
				if (confirm("Are you sure to delete \"" + nth["_title"] + "\"?")) {
					NProgress.start();
					$.ajax({
						method: "post",
						url: global_server + "/projectx-server/in-client/delete-lead-finder",
						contentType: "application/json",
						headers: { "Authorization": this.generalSettings["license"] },
						data: JSON.stringify({ lead: nth["_leadfinder"] })
					})
					.done(function(data, textStatus, xhr){
						// console.log(data);
						// console.log(textStatus);
						// console.log(xhr);

						alert(data["message"]);

						projectx.get_leadFinders();
						NProgress.done();
					});
				}
			},
			get_leadFinders: function(){
				// get email templates
				NProgress.start();
				$.ajax({
					method: "get",
					url: global_server + "/projectx-server/get-lead-finder-schedules",
					contentType: "application/json",
					data: { owner: this.generalSettings["license"], set: "*" }
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					projectx.scheduleList = data["data"];
					NProgress.done();
				});
			},
			stage_leadFinder: function(nth){
				try{ 
					// check if recipient is json format - send invite
					this.currentSchedule_LeadFinder_sendConnection["recipients"]["searchKey"] = JSON.parse(nth["_recipients"])["searchKey"];
					this.currentSchedule_LeadFinder_sendConnection["recipients"]["n_recipients"] = JSON.parse(nth["_recipients"])["n_recipients"];
					if(this.currentSchedule_LeadFinder_sendConnection["recipients"]["searchKey"] == undefined || 
						this.currentSchedule_LeadFinder_sendConnection["recipients"]["n_recipients"] == undefined){
						throw Error;
					}
					else{
						this.currentSchedule_LeadFinder_sendConnection["lead"] = nth["_leadfinder"];
						this.currentSchedule_LeadFinder_sendConnection["title"] = nth["_title"];
						this.currentSchedule_LeadFinder_sendConnection["template_id"] = nth["_template"];
						this.currentSchedule_LeadFinder_sendConnection["trigger"] = nth["_trigger"];
					}
					$('a[href="#scheduler_connectionInvite"]').tab("show")
				}
				catch(err){
					// this is a in Network message
					this.currentSchedule_LeadFinder_sendMessage["lead"] = nth["_leadfinder"];
					this.currentSchedule_LeadFinder_sendMessage["title"] = nth["_title"];
					this.currentSchedule_LeadFinder_sendMessage["template_id"] = nth["_template"];
					this.currentSchedule_LeadFinder_sendMessage["trigger"] = nth["_trigger"];
					this.currentSchedule_LeadFinder_sendMessage["recipients"] = JSON.parse(nth["_recipients"]);
					$('a[href="#scheduler_inNetworkMessages"]').tab("show")
				}
			},
			unstage_leadFinder: function(){
				$('a[href="#schedules_leadfinder"]').tab("show");

				// unstage sending invites
				this.currentSchedule_LeadFinder_sendConnection["lead"] = "";
				this.currentSchedule_LeadFinder_sendConnection["title"] = "";
				this.currentSchedule_LeadFinder_sendConnection["template_id"] = "";
				this.currentSchedule_LeadFinder_sendConnection["trigger"] = "";
				this.currentSchedule_LeadFinder_sendConnection["recipients"]["searchKey"] = "";
				this.currentSchedule_LeadFinder_sendConnection["recipients"]["n_recipients"] = 0;

				// unstage in-network message
				this.currentSchedule_LeadFinder_sendMessage["lead"] = "";
				this.currentSchedule_LeadFinder_sendMessage["title"] = "";
				this.currentSchedule_LeadFinder_sendMessage["template_id"] = "";
				this.currentSchedule_LeadFinder_sendMessage["trigger"] = "";
				this.currentSchedule_LeadFinder_sendMessage["recipients"] = [];				
			},
			getTriggerOptions: function(){
				// get trigger options
				NProgress.start();
				$.ajax({
					method: "get",
					url: global_server + "/projectx-server/get-triggers",
					contentType: "application/json"
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"] == null || data["data"] == "undefined" || data["data"] === undefined || data["data"].length == 0 || data["data"].constructor !== Array){
						alert("Sorry, loading triggers failed.");
					}
					else{ projectx.triggerOptions = data["data"]; }

					NProgress.done();
				});
			},
			extractComments: function(comment){
				try{ return JSON.parse(comment)["comments"].replace("\n", /<br>/g).replace("\t", /&emsp;/g); }
				catch(err){ return "" }
			},
		// trigger FACTOR!
			syncClock: function(){
				// don't implement loading
				setInterval(function(){
					$.ajax({
						method: "get",
						url: global_server + "/projectx-server/check-uniform-time",
						contentType: "application/json",
						data: { client: projectx.generalSettings["license"] }
					})
					.done(function(data, textStatus, xhr){
						// console.log(data);
						// console.log(textStatus);
						// console.log(xhr);

						// its another day!
						if(data["date"] != projectx.today){ 
							projectx.triggerToday = false; 
							projectx.today = data["date"];
							projectx.nextTrigger = data["next"];

							console.log("Trigger reset: " + data["date"]);
						}

						projectx.enableRefresh = data["status"];
						if(data["status"] == true){
							if(projectx.triggerToday == false){
								// fire triggers NOW!!!! -----------------------------------> ONLY WORKS FOR DAILY 
								projectx.triggerToday = true;
								
								for (var i = 0; i < data["schedule"].length; i++) {
									// check last trigger - if null - trigger now - else - check if ----- DAILY / WEEKLY / etc.

									// check the latest date of trigger
									if(data["schedule"][i]["_latest"] != data["date"]){
										try{ 
											searchKey = JSON.parse(data["schedule"][i]["_recipients"])["searchKey"];
											countRecipients = JSON.parse(data["schedule"][i]["_recipients"])["n_recipients"];
											if(searchKey == undefined || countRecipients == undefined){ throw Error; }
											else{
												var searchTime = (new Date().getTime());
												var searchMethod = "SWITCH_SEARCH_VERTICAL";

												projectx.trigger_sendInvite(countRecipients, searchKey, searchMethod, searchTime, i, data);
											}
										}
										catch(err){
											var recipients = JSON.parse(data["schedule"][i]["_recipients"]);
											var myNetwork = projectx.linkedin_spyders["linkedin_networkRecipients"];
											for (var l = 0; l < recipients.length; l++) {
												// find name 
												for (var k = 0; k < myNetwork.length; k++) {
													if(myNetwork[k]["entityUrn"] == ("urn:li:fs_relConnection:" + recipients[l])){
														var email = data["schedule"][i]["_content"].replace("{name}", myNetwork[k]["miniProfile"]["firstName"]).replace(/<br>/g, "\n").replace(/&emsp;/g, /\t/g)
														var profileNth = myNetwork[k]["miniProfile"]["publicIdentifier"]
														projectx.sendMessage(recipients[l], email, profileNth, myNetwork[k]);
													}
												}
											}
										}

										try{ projectx.recordLog(data["schedule"][i]["_leadfinder"], data["date"]); }
										catch(Error){ console.log(Error); }
									}
								}
							}
						}

						projectx.triggerCountDown = data["time"];
						projectx.myTime = new Date();
					});
				}, 1000); // This is costly!
			},
			trigger_sendInvite: function(countRecipients, searchKey, searchMethod, searchTime, i, data){
				$.ajax({
					method: "get",
					url: "https://www.linkedin.com/voyager/api/search/cluster?count=40&guides="+encodeURI(projectx.linkedin_spyders["search_filter"])+"&keywords="+encodeURI(searchKey)+"&origin="+searchMethod+"&q=guided&searchId="+searchTime+"&start=0",
					headers: {
						"csrf-token": projectx.getCookie("JSESSIONID", projectx.cookies["linkedin"]),
						"x-li-track":'{"clientVersion":"1.0.*","osName":"web","clientTimestamp":'+searchTime+',"timezoneOffset":8,"deviceFormFactor":"DESKTOP"}'
					}
				})
				.then(function(data_chain1, textStatus_chain1, xhr_chain1){
					if(data_chain1['elements'].length > 0){ projectx.inviteeCollection = data_chain1["elements"][0]["elements"]; }

					return $.ajax({
						method: "get",
						url: "https://www.linkedin.com/voyager/api/search/cluster?count=40&guides="+encodeURI(projectx.linkedin_spyders["search_filter"])+"&keywords="+encodeURI(searchKey)+"&origin="+searchMethod+"&q=guided&searchId="+searchTime+"&start=1",
						headers: {
							"csrf-token": projectx.getCookie("JSESSIONID", projectx.cookies["linkedin"]),
							"x-li-track":'{"clientVersion":"1.0.*","osName":"web","clientTimestamp":'+searchTime+',"timezoneOffset":8,"deviceFormFactor":"DESKTOP"}'
						}
					})
				})
				.then(function(data_chain2, textStatus_chain2, xhr_chain2){
					if(data_chain2['elements'].length > 0){ projectx.inviteeCollection = projectx.inviteeCollection.concat(data_chain2["elements"][0]["elements"]); }

					return $.ajax({
						method: "get",
						url: "https://www.linkedin.com/voyager/api/search/cluster?count=40&guides="+encodeURI(projectx.linkedin_spyders["search_filter"])+"&keywords="+encodeURI(searchKey)+"&origin="+searchMethod+"&q=guided&searchId="+searchTime+"&start=2",
						headers: {
							"csrf-token": projectx.getCookie("JSESSIONID", projectx.cookies["linkedin"]),
							"x-li-track":'{"clientVersion":"1.0.*","osName":"web","clientTimestamp":'+searchTime+',"timezoneOffset":8,"deviceFormFactor":"DESKTOP"}'
						}
					})
				})
				.then(function(data_chain3, textStatus_chain3, xhr_chain3){
					if(data_chain3['elements'].length > 0){ projectx.inviteeCollection = projectx.inviteeCollection.concat(data_chain3["elements"][0]["elements"]); }

					return $.ajax({
						method: "get",
						url: "https://www.linkedin.com/voyager/api/search/cluster?count=40&guides="+encodeURI(projectx.linkedin_spyders["search_filter"])+"&keywords="+encodeURI(searchKey)+"&origin="+searchMethod+"&q=guided&searchId="+searchTime+"&start=3",
						headers: {
							"csrf-token": projectx.getCookie("JSESSIONID", projectx.cookies["linkedin"]),
							"x-li-track":'{"clientVersion":"1.0.*","osName":"web","clientTimestamp":'+searchTime+',"timezoneOffset":8,"deviceFormFactor":"DESKTOP"}'
						}
					})
				})
				.done(function(data2, textStatus2, xhr2){
					// console.log(data2);
					// console.log(textStatus2);
					// console.log(xhr2);

					if(data2['elements'].length > 0){ projectx.inviteeCollection = projectx.inviteeCollection.concat(data2["elements"][0]["elements"]); }
					if(projectx.inviteeCollection.length > 0){
						var searchResult = projectx.inviteeCollection;
						var countStop = 0;
						// get all ids
						for (var j = 0; j < searchResult.length; j++) {
							if(!projectx.checkRelationship(searchResult[j]) && countStop < countRecipients){
								countRecipients++;
								try{
									var email = data["schedule"][i]["_content"].replace("{name}", searchResult[j]["hitInfo"]["com.linkedin.voyager.search.SearchProfile"]["miniProfile"]["firstName"]).replace(/<br>/g, "\n").replace(/&emsp;/g, /\t/g)
									projectx.sendConnection_Invite(searchResult[j], email);
								}
								catch(error){ console.log(error); }
							}
						}

						projectx.inviteeCollection = [];
					}
				});
			},
			sendConnection_Invite: function(nth, email){
				var thisClient = nth;
				var searchTime = (new Date().getTime());

				// YOU SHOULD NOT AUTOMATE SENDING CONNECTIONS TO CONNECTIONS TO -- ["distance"]["value"] = "OUT_OF_NETWORK"
				//	-- your account might get banned
				// check this link for good practices ---> http://theundercoverrecruiter.com/linkedin-invitations/
				var parentNth = nth["hitInfo"]["com.linkedin.voyager.search.SearchProfile"]["miniProfile"]
				$.ajax({
					method: "post",
					url: "https://www.linkedin.com/voyager/api/growth/normInvitations",
					contentType: "application/json; charset=UTF-8",
					headers: {
						"csrf-token": this.getCookie("JSESSIONID", this.cookies["linkedin"]),
						"x-li-track":'{"clientVersion":"1.0.*","osName":"web","clientTimestamp":'+searchTime+',"timezoneOffset":8,"deviceFormFactor":"DESKTOP"}'
					},
					data: JSON.stringify({
						"trackingId": thisClient["trackingId"],
						"message": email,
						"invitations": [],
						"excludeInvitations": [],
						"invitee": {
							"com.linkedin.voyager.growth.invitation.InviteeProfile": {
								"profileId": String(thisClient["hitInfo"]["com.linkedin.voyager.search.SearchProfile"]["id"])
							}
						}
					}),
					statusCode: {
						409: function() {
							var name = parentNth["firstName"]
							if(name == ""){ name = "this user."; }

							console.log("You have already invited " + name);
							projectx.profileView(parentNth["publicIdentifier"], thisClient);
							projectx.recordAnalytics("sendInvite", thisClient, 409);
							// log - sending message - recipient and email template
						}
					}
				})
				.done(function(data, textStatus, xhr){
					// console.log(textStatus);
					projectx.profileView(parentNth["publicIdentifier"], thisClient);
					projectx.recordAnalytics("sendInvite", thisClient, xhr.status);
					// log - sending message - recipient and email template
					console.log("invite - " + xhr.status + " - " + parentNth["firstName"]);
				});
			},
			sendMessage: function(recipient, email, profileNth, nthObj){
				var searchTime = (new Date().getTime());
				
				$.ajax({
					async: true,
					crossDomain: true,
					processData: false,
					method: "post",
					url: "https://www.linkedin.com/voyager/api/messaging/conversations?action=create",
					contentType: "application/json; charset=UTF-8",
					headers: {
						"csrf-token": this.getCookie("JSESSIONID", this.cookies["linkedin"]),
						"x-li-track":'{"clientVersion":"1.0.*","osName":"web","clientTimestamp":'+searchTime+',"timezoneOffset":8,"deviceFormFactor":"DESKTOP"}'
					},
					data: JSON.stringify({
						"conversationCreate": {
							"recipients": [
								// insert multiple recipients here...
								// CANNOT do since every email has to be custom -
								recipient
							],
							"eventCreate": {
								"value": {
									"com.linkedin.voyager.messaging.create.MessageCreate": {
										"attachments": [],
										"body": email,
									}
								}
							},
							"subtype": "MEMBER_TO_MEMBER"
						}
					}),
					statusCode: {
						500: function() { 
							console.log("You have been blocked by " + recipient + " - through linkedin"); 
							// log - sending message - recipient and email template
							projectx.profileView(profileNth, nthObj);
							projectx.recordAnalytics("sendMessage", nthObj, 500);
						}
					}
				})
				.done(function(data, textStatus, xhr){
					// console.log(textStatus);
					projectx.profileView(profileNth, nthObj);
					projectx.recordAnalytics("sendMessage", nthObj, xhr.status);
					// log - sending message - recipient and email template
					console.log("message - " + xhr.status + " - " + profileNth);
				});
			},
			recordLog: function(leadfinder, date){
				$.ajax({
					method: "get",
					url: global_server + "/projectx-server/log-trigger",
					contentType: "application/json",
					data: { leadfinder: leadfinder, date: date }
				})
			},
			profileView: function(nth, nthObj){
				try{
					$.ajax({
						method: "post",
						url: "https://www.linkedin.com/li/track",
						contentType: "application/json",
						data: JSON.stringify([
							{
								"eventInfo": {"topicName":"ProfileViewEvent","eventName":"ProfileViewEvent","appId":"com.linkedin.flagship3.d_web"},
								"eventBody": {
									"requestHeader": {
										"path":"https://www.linkedin.com/in/"+nth+"/",
										"referer":null,
										"pageKey":"d_flagship3_profile_view_base",
										"trackingCode":"d_flagship3_me_wvm_v2",
										"interfaceLocale":"en_US"
									},
									"header": {
										"pageInstance": {
											"pageUrn":"urn:li:page:d_flagship3_profile_view_base",
											"trackingId":"37SveInATfW00l6Qq+X4KQ=="
										},
										"time": 1495789027406,
										"clientApplicationInstance": {
											"applicationUrn": "urn:li:application:(voyager-web,voyager-web)",
											"version":"1.0.9564", 
											"trackingId":[98,-45,33,51,-89,96,72,-20,-118,-99,82,78,-127,-90,-70,62]
										}
									},
									"viewerPrivacySetting": "F",
									"networkDistance":-1,
									"vieweeMemberUrn":"urn:li:member:574144697",
									"profileTrackingId":"TfZV9vTRTJ2/wiMVM66hOQ==",
									"entityView": {
										"viewType":"profile-view",
										"viewerId":534285108,
										"targetId":574144697
									}
								}
							}, { 
								"eventInfo": {
									"topicName":"PageViewEvent",
									"eventName":"PageViewEvent",
									"appId":"com.linkedin.flagship3.d_web"
								},
								"eventBody":{
									"requestHeader":{
										"pageKey":"d_flagship3_profile_view",
										"trackingCode":"d_flagship3_me_wvm_v2",
										"path":"https://www.linkedin.com/in/"+nth+"/",
										"referer":null,
										"interfaceLocale":"en_US"
									},
									"header":{
										"pageInstance":{
											"pageUrn":"urn:li:page:d_flagship3_profile_view_base",
											"trackingId":"37SveInATfW00l6Qq+X4KQ=="
										},
										"time":1495789027407,
										"clientApplicationInstance":{
											"applicationUrn":"urn:li:application:(voyager-web,voyager-web)",
											"version":"1.0.9564",
											"trackingId":[98,-45,33,51,-89,96,72,-20,-118,-99,82,78,-127,-90,-70,62]
										}
									},
									"pageType":"ajax",
									"trackingInfo":{
										"0":"unknown",
										"1":"phone_web",
										"2":"1.0.9564",
										"3":"UNK",
										"4":"unknown",
										"5":"en_US",
										"10":"1495789027406",
										"osVersion":"unknown",
										"osName":"phone_web",
										"appVersion":"1.0.9564",
										"carrier":"UNK",
										"deviceModel":"unknown",
										"locale":"en_US",
										"clientTimestamp":"1495789027406"
									},
									"trackingCode":"d_flagship3_me_wvm_v2"
								}
							},{
								"eventInfo":{
									"eventName":"ExternalTrackingPageViewEvent",
									"appId":"com.linkedin.flagship3.d_web"
								},
								"eventBody":{
									"pageViewPageKey":"d_flagship3_profile_view",
									"pageViewPath":"/in/"+nth+"/",
									"initiator":"APP",
									"providers":["COMSCORE"],
									"requestHeader":{
										"pageKey":"d_flagship3_profile_view_base"
									},
									"samplingFraction":1
								}
							}])
					})
					.done(function(data, textStatus, xhr){
						// console.log(textStatus);
						projectx.recordAnalytics("profileView", nthObj, xhr.status);
						// log - sending message - recipient and email template
					});
				}
				catch(err){ console.log("Error: Unexpected format of profile 'nth' " + err.message); }
			},
		// Analytics
			recordAnalytics: function(funcAuthor, nth, status){
				$.ajax({
					method: "post",
					url: global_server + "/projectx-server/new-analytics-log",
					contentType: "application/json",
					headers: { "Authorization": this.generalSettings["license"] },
					data: JSON.stringify({ funcAuthor: funcAuthor, nth: nth, status: status })
				})
				.done(function(data, textStatus, xhr){
					console.log("Analytics - " + funcAuthor + " - " + status);
				});
			},
			getAnalytics: function(){
				var dateSet = [];

				// assemble 7 days
				for (var i = 0; i < 7; i++) {
					var date = new Date();
					date.setDate(date.getDate() - i);
					dateSet.push(date.toISOString().substring(0, 10));
				}

				NProgress.start();
				$.ajax({
					method: "get",
					url: global_server + "/projectx-server/get-analytics-log",
					contentType: "application/json",
					data: { 
						"owner": this.generalSettings["license"],
						"dates": dateSet
					}
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					projectx.timeLine_Data["labels"] = dateSet.reverse();

					// segregate the result data by 'func'
					for (var i = 0; i < data["data"].length; i++) {
						var currData = data["data"][i];
						
						// find proper date
						for (var j = 0; j < projectx.timeLine_Data["labels"].length; j++) {
							if(projectx.timeLine_Data["labels"][j] == currData["date"]){ 
								var temp = {}
								if(currData["status"] >= 200 && currData["status"] <= 299){
									if(currData["func"] == "sendInvite"){ temp = projectx.timeLine_Data["Invites"]; }
									else if(currData["func"] == "sendMessage"){ temp = projectx.timeLine_Data["Messages"]; }
									else if(currData["func"] == "profileView"){ temp = projectx.timeLine_Data["VProfile"]; }
									temp[j] = currData["score"];
								}
							}
						}
					}

					NProgress.done();
				});
			},
			getAnalytics_daily: function(date, func, storage, users){
				NProgress.start();
				$.ajax({
					method: "get",
					url: global_server + "/projectx-server/get-analytics-daily",
					contentType: "application/json",
					data: { "dataSet": [this.generalSettings["license"], date, func] }
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["data"].length > 0){
						var success = 0;
						var error = 0;
						var unknown = 0;

						for (var i = 0; i < data["data"].length; i++) {
							var temp = data["data"][i];
							if(temp["status"] >= 200 && temp["status"] <= 299){ success++; }
							else if(temp["status"] >= 400 && temp["status"] <= 599){ error++; }
							else{ unknown++; }

							try{ 
								var nth = JSON.parse(temp["nth"]);
								var nth_name = nth["hitInfo"]["com.linkedin.voyager.search.SearchProfile"]["miniProfile"];
								users.push({"name": nth_name["firstName"] + " " + nth_name["lastName"], "status": temp["status"]});
							}
							catch(err1){ 
								try{
									var nth = JSON.parse(temp["nth"]);
									var nth_name = nth["miniProfile"];
									users.push({"name": nth_name["firstName"] + " " + nth_name["lastName"], "status": temp["status"]});
								}
								catch(err2){ console.log("skipped: " + temp["nth"]); }
							}
						}

						storage = [success, error, unknown];
					}
					else{ 
						storage = [0, 0, 0];
						users = [];
					}

					if(func == "sendInvite"){
						projectx.invites_data = storage;
						projectx.invites_data_users = users;
					}
					if(func == "sendMessage"){
						projectx.sendMessage_data = storage;
						projectx.sendMessage_data_users = users;
					}
					if(func == "profileView"){
						projectx.profileView_data = storage;
						projectx.profileView_data_users = users;
					}

					NProgress.done();
				});
			},
		// twitter handler - updates
			twitterHandles: function(nth){
				// Initialize variables.
				var publicIdentifier = nth["hitInfo"]["com.linkedin.voyager.search.SearchProfile"]["miniProfile"]["publicIdentifier"]; // for linkedin misc profileContactInfo
				// Routine for to get twitter account in linkedin.
				var linkedinContactInfo = projectx.profileContactInfo(publicIdentifier);
				// Initialize Twitter variables.
				var twitterClient = new Twitter({
				  consumer_key: twitterconfig["consumer_key"],
				  consumer_secret: twitterconfig["consumer_secret"],
				  access_token_key: twitterconfig["access_token_key"],
				  access_token_secret: twitterconfig["access_token_secret"]
				});
				var twitterScreenName = linkedinContactInfo["twitterHandles"][0]["name"];
				// Follow twitter.
				projectx.twitterFollow(twitterClient, twitterScreenName);
				// Search latest tweets.
				projectx.twitterSearchTweet(twitterClient, twitterScreenName, 5); // last params = tweet count
				// Like twitter tweet/s
			},
		// twitter misc
			twitterFollow: function(twitterClient, twitterScreenName){
				var params = {screen_name: twitterScreenName}
				twitterClient.post('friendships/create', params, function(error, tweets, response) {
				  if (!error) {
				    // console.log(JSON.stringify(tweets));
				  }
				});
			},
			twitterSearchTweet: function(twitterClient, twitterScreenName, twitterTweetCount){
				var params = {screen_name:twitterScreenName, count:twitterTweetCount};
				twitterClient.get('statuses/user_timeline', params, function(error, tweets, response) {
				  if (!error) {
				    // console.log(JSON.stringify(tweets));
						var tweetsnum = Object.keys(tweets).length;
						for (var i = 0; i < tweetsnum; i++) {
							var twitterTweetId = tweets[i]["id_str"];
							// Routine to like tweet
							projectx.twitterLikeTweet(twitterClient, twitterTweetId);
						}
				  }
				});
			},
			twitterLikeTweet: function(twitterClient, twitterTweetId){
				var params = {id:twitterTweetId};
				twitterClient.post('favorites/create', params, function(error, tweets, response) {
				  if (!error) {
				    console.log(JSON.stringify(tweets));
				  }
				});
			},
		// MISC - functions
			checkRelationship: function(nth){
				try{ 
					var thisObj = nth["hitInfo"]["com.linkedin.voyager.search.SearchProfile"]["profileActions"]["primaryAction"]["action"];
					if("com.linkedin.voyager.identity.profile.actions.Message" in thisObj){
						// if connected - Primary action is message only
						return true;
					}
					else{
						// if not connected - Primary action is send connection requests
						return false;
					}
				}
				catch(err){ return false; }
				// default value to stay safe
				return false;
			},
			extractID: function(inId){ return inId.split(":")[3]; },
		// xpress
			xpress_createInvites: function(){
				console.log(this.xpress_invites);

				// XPRESS Save Lead Finder
				NProgress.start();
				$.ajax({
					method: "post",
					url: global_server + "/projectx-server/in-client/insert-xpress-lead-finder",
					contentType: "application/json",
					headers: { "Authorization": this.generalSettings["license"] },
					data: JSON.stringify(this.xpress_invites)
				})
				.done(function(data, textStatus, xhr){
					// console.log(data);
					// console.log(textStatus);
					// console.log(xhr);

					if(data["message"].indexOf("Error") == -1){ alert(data["message"]); }
					else{ alert("Error! Don't leave a blank in the input fields."); }

					// clear current template
					if(data["data"] != null){ 
						projectx.invite_listText = "";
						projectx.invite_listLength = 0;
						projectx.xpress_view = "xpress-Step3";
						projectx.xpress_invites = [];
					}

					projectx.get_leadFinders();
					NProgress.done();
				});
			},
			xpress_countLength: function(){
				if(this.invite_listText.split("\n").length > 0){ this.invite_listLength = true; }
				else{ this.invite_listLength = false; }
			}
	},
	computed:{
		// email templates
			emailTemplateList: function(){
				var searchResult = [];
				if(this.search_emailTemplate.trim() == ""){ searchResult = this.emailTemplates; }
				else{
					for (var i = 0; i < this.emailTemplates.length; i++) {
						if(this.emailTemplates[i]["_title"].toLowerCase().indexOf(this.search_emailTemplate.toLowerCase()) + 1 ){ searchResult.push(this.emailTemplates[i]); }
						else if(this.emailTemplates[i]["_details"].toLowerCase().indexOf(this.search_emailTemplate.toLowerCase()) + 1 ){ searchResult.push(this.emailTemplates[i]); }
						else if(this.emailTemplates[i]["_log_created"].toLowerCase().indexOf(this.search_emailTemplate.toLowerCase()) + 1 ){ searchResult.push(this.emailTemplates[i]); }
					}
				}
				return searchResult
			},
		// xpress
			invite_listText_Table: function(){
				var temp = String(this.invite_listText).split("\n");
				for (var i = 0; i < temp.length; i++) { 
					id = Date.now();
					this.xpress_invites[i] = {
						"title": "Xpress - " + (id++), 
						"template": "", 
						"recipient": {
							"searchKey": temp[i],
							"n_recipients": 0
						},
						"trigger": ""
					}; 
				}
				return temp;
			},
		// myTime
			timeIn_24format: function(){
				return this.myTime.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: false, hour24: true });
			},
			timeIn_12format: function(){
				return this.myTime.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', second: 'numeric', hour12: true, hour24: false });
			},
	},
	mounted: function(){
		// init functions
		this.loadCredentials();
		this.openNav();

		// try loading cookies if any
		try { this.loadCookieFile(); }
		catch(Exception){}

		// check current view
		this.showView("home");
		this.liveSearchLinkedin_getNetwork(0);
		this.getTriggerOptions();

		// resize
		// window.onresize = function(event) { location.reload(); }
	}
});