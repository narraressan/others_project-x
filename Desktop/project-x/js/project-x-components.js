
// component: General Direction


// component: STEP 1 - Activate Account
	// a - settings
	// b - linkedin account


// component: STEP 2 - Schedule
	// a - select schedule - "profile scanner" or "lead finder"
	// b - fillup form
	// c - select "email template" or "write on the fly and save as template"
	// select recipients (copy paste search for mass creation of schedule feature)



// Analytics
	Vue.component('line-chart', {
		extends: VueChartJs.Line,
		props: ['data', 'options'],
		// template: '<div>A custom component! {{ message }}</div>',
		computed: { chartData: function(){ return this.data; } },
		methods: {
			renderChartNow: function(){
				this.renderChart({
					labels: this.chartData["labels"],
					datasets: [
						{
							label: "Invites",
							fill: false,
							lineTension: 0.1,
							backgroundColor: "#76a8f7",
							borderColor: "#76a8f7",
							pointBackgroundColor: "#fff",
							pointBorderWidth: 1,
							pointHoverRadius: 5,
							pointHoverBackgroundColor: "#76a8f7",
							pointHoverBorderWidth: 2,
							pointRadius: 1,
							pointHitRadius: 10,
							data: this.chartData["Invites"],
							spanGaps: false,
						}, {
							label: "Messages",
							fill: false,
							lineTension: 0.1,
							backgroundColor: "#f7758d",
							borderColor: "#f7758d",
							pointBackgroundColor: "#fff",
							pointBorderWidth: 1,
							pointHoverRadius: 5,
							pointHoverBackgroundColor: "#f7758d",
							pointHoverBorderWidth: 2,
							pointRadius: 1,
							pointHitRadius: 10,
							data: this.chartData["Messages"],
							spanGaps: false,
						}, {
							label: "View Profile",
							fill: false,
							lineTension: 0.1,
							backgroundColor: "#38ea73",
							borderColor: "#38ea73",
							pointBackgroundColor: "#fff",
							pointBorderWidth: 1,
							pointHoverRadius: 5,
							pointHoverBackgroundColor: "#38ea73",
							pointHoverBorderWidth: 2,
							pointRadius: 1,
							pointHitRadius: 10,
							data: this.chartData["VProfile"],
							spanGaps: false,
						}
					]
				}, this.options);
			}
		},
		mounted(){ this.renderChartNow();},
		watch: {
			data: function () {
				this._chart.destroy();
				this.renderChartNow();
			}
		}
	})

	Vue.component('pie-chart', {
		extends: VueChartJs.Pie,
		props: ['data', 'options'],
		// template: '<div>A custom component! {{ message }}</div>',
		computed: { chartData: function(){ return this.data; } },
		methods: {
			renderChartNow: function(){
				this.renderChart({ labels: ['Success', 'Failed', 'Unknown Errors'], datasets: [{ data: this.chartData, backgroundColor: ["#52ff3f","#FF6384","#acb7aa"] }]}, this.options);
			}
		},
		mounted(){ this.renderChartNow();},
		watch: {
			data: function () {
				this._chart.destroy();
				this.renderChartNow();
			}
		}
	});